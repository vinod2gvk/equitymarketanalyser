<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Add Company</title>
	</head>

	
	
	<!doctype html>
<title>Example</title>
<style>
  * {
   box-sizing: border-box; 
  }
  body {
    margin: 0;
  }
  #main {
    display: flex;
    min-height: calc(100vh - 40vh);
  }
  #main > article {
    flex: 1;
  }
  #main > nav, 
  #main > aside {
    flex: 0 0 20vw;
    background: beige;
  }
  #main > nav {
    order: -1;
  }
  header, footer, article, nav, aside {
    padding: 1em;
  }
  header, footer {
    background: yellowgreen;
    height: 20vh;
  }
</style>
<body>
  <header><%@include file="header.jsp" %></header>
  <div id="main">
    <article>
    
    
      <%@include file="header.jsp" %>
		<h2>Add Company Data</h2>
		<form:form method="POST" action="/EquityAnalyser/save.html">
	   		<table>
			    <tr>
			        <td><form:label path="name">Company Name:</form:label></td>
			        <td><form:input path="name" value="${company.name}"/></td>
			    </tr>
			    <tr>
			        <td><form:label path="code">Company code:</form:label></td>
			        <td><form:input path="code" value="${company.code}"/></td>
			    </tr>
			  
			    <tr>
			      <td colspan="2"><input type="submit" value="Submit"/></td>
		      </tr>
			</table> 
		</form:form>
		
  <c:if test="${!empty companies}">
		<h2>List Companies</h2>
	<table align="left" border="1">
		<tr>
			<th>Company Name</th>
			<th>Company Code</th>
			
		</tr>

		<c:forEach items="${companies}" var="company">
			<tr>
				<td><c:out value="${company.name}"/></td>
				<td><c:out value="${company.code}"/></td>
				
				<td align="center"><a href="edit.html?code=${company.code}">Edit</a> | <a href="delete.html?code=${company.code}">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
</c:if>
    
    </article>
    <nav><%@include file="navigation.jsp" %></nav>
    <aside><%@include file="widget.jsp" %></aside>
  </div>
  <footer><%@include file="footer.jsp" %></footer>
</body> 
</html>