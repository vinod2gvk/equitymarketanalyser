
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
	<head>
		<title>Company Filter</title>
	</head>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="/resources/js/csvExport.js"></script>
<style>
  * {
   box-sizing: border-box; 
  }
  body {
    margin: 0;
  }
  #main {
    display: flex;
    min-height: calc(100vh - 40vh);
  }
  #main > article {
    flex: 1;
  }
  #main > nav, 
  #main > aside {
    flex: 0 0 20vw;
    background: beige;
  }
  #main > nav {
    order: -1;
  }
  header, footer, article, nav, aside {
    padding: 1em;
  }
  header, footer {
    background: yellowgreen;
    height: 20vh;
  }
</style>
<body>
  <header><%@include file="header.jsp" %></header>
  <div id="main">
    <article>
    
    
		<h2>Company Filter</h2>
		<form method="post" action="/analysis/companyfilter.html">

			<table>

				<tr>

					<td>Price diff percentage(%)</td>

					<td><input type="number" name="pricePercentage" size="4" value="200"/>
				</tr>
				<tr>
					<td>Min. PE Ratio</td>

					<td><input type="number" name="minPERatio" size="3" value="1"/>
				</tr>
				<tr>
					<td>Max. PE Ratio</td>

					<td><input type="number" name="maxPERatio" size="3" value="20"/>
				</tr>
                  <tr>
					
					<td colspan="2"><input type="submit" value="Submit"/>

				</tr>

			</table>

		</form>


		
<c:if test="${!empty companies}">
<h2>List of filtered Companies</h2>
	<table align="left" border="1">
		<tr>
			<th>Company Code</th>
			<th>PE Ratio</th>
			<th>EPS</th>
			<th>Sales in crores</th>
			<th>Last Bonus</th>
			<th>Last Dividend</th>
			<th>Face Value</th>
			<th>Current Value</th>
			
		</tr>

		<c:forEach items="${companies}" var="company">
			<tr>
				<td><c:out value="${company.code}"/></td>
				<td><c:out value="${company.peRatio}"/></td>
				<td><c:out value="${company.eps}"/></td>
				<td><c:out value="${company.sales}"/></td>
				<td><c:out value="${company.lastBonus}"/></td>
				<td><c:out value="${company.lastDivided}"/></td>
				<td><c:out value="${company.faceValue}"/></td>
				<td><c:out value="${company.currentValue}"/></td>				
			</tr>
		</c:forEach>
	</table>
	<button id="export" class="btn btn-primary">Export</button>
</c:if>
    
    </article>
    <nav><%@include file="navigation.jsp" %></nav>
    <aside><%@include file="widget.jsp" %></aside>
  </div>
  <footer><%@include file="footer.jsp" %></footer>
  
  <script>
$( "#export" ).click(function() {
  $('table').csvExport();
});
</script>
</body> 
</html>