package com.marketanalyser.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.marketanalyser.bean.CompanyBean;
import com.marketanalyser.model.Company;
import com.marketanalyser.service.ImportService;
import com.marketanalyser.service.CompanyService;
import com.marketanalyser.service.ReadRediff;

/**
 * @author Vinod
 *
 */
@Controller
public class CompanyController {
	
	@Autowired
	private CompanyService companyService;
	

	
	@Autowired
	private ReadRediff readRediff;
	
//	@RequestMapping(value = "/save", method = RequestMethod.POST)
//	public ModelAndView saveCompany(@ModelAttribute("command") CompanyBean companyBean, 
//			BindingResult result) {
//		Company company = prepareModel(companyBean);
//		companyService.addCompany(company);
//		return new ModelAndView("redirect:/add.html");
//	}

	@RequestMapping(value="/companies", method = RequestMethod.GET)
	public ModelAndView listCompanies() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("companies",  prepareListofBean(companyService.listCompanies()));
		return new ModelAndView("companiesList", model);
	}

	@RequestMapping(value="/getDetails", method = RequestMethod.GET)
	public ModelAndView details() throws IOException {
		
		List<Company> companies = companyService.listCompanies();
		
		for(Company company:companies) {
			
			Document doc = Jsoup.connect("http://money.rediff.com/companies/"+company.getCompanyCode()).get();
			Element currentPriceElement = doc.select("#ltpid").first();
			String price = currentPriceElement.text();
			
			Elements details = doc.select("#div_rcard_more .floatR");
			
			
			String peRatioNode = details.get(0).text();
			String epsNode = details.get(1).text();
			String salesNode = details.get(2).text();
			String faceValueNode = details.get(3).text();
			String lastBonusNode = details.get(5).text();
			String lastDividendNode = details.get(6).text();

			
		}
		
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("companies",  prepareListofBean(companyService.listCompanies()));
		return new ModelAndView("companiesList", model);
	}
	
	@RequestMapping(value="/readCSV", method = RequestMethod.GET)
	public ModelAndView readCompanies() {
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("companies",  prepareListofBean(companyService.listCompanies()));
		return new ModelAndView("companiesList", model);
	}
	
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView addCompany(@ModelAttribute("command")  CompanyBean companyBean,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("companies",  prepareListofBean(companyService.listCompanies()));
		return new ModelAndView("addCompany", model);
	}
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView welcome() {
		return new ModelAndView("index");
	}
	
//	@RequestMapping(value = "/delete", method = RequestMethod.GET)
//	public ModelAndView editCompany(@ModelAttribute("command")  CompanyBean companyBean,
//			BindingResult result) {
//		companyService.deleteCompany(prepareModel(companyBean));
//		Map<String, Object> model = new HashMap<String, Object>();
//		model.put("company", null);
//		model.put("companies",  prepareListofBean(companyService.listCompanies()));
//		return new ModelAndView("addCompany", model);
//	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView deleteCompany(@ModelAttribute("command")  CompanyBean companyBean,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("company", prepareCompanyBean(companyService.getCompany(companyBean.getCode())));
		model.put("companies",  prepareListofBean(companyService.listCompanies()));
		return new ModelAndView("addCompany", model);
	}
	
	private Company prepareModel(CompanyBean companyBean){
		Company company = new Company();
		company.setCompanyName(companyBean.getName());
		company.setCompanyCode(companyBean.getCode());
		return company;
	}
	
	private List<CompanyBean> prepareListofBean(List<Company> companies){
		List<CompanyBean> beans = null;
		if(companies != null && !companies.isEmpty()){
			beans = new ArrayList<CompanyBean>();
			CompanyBean bean = null;
			for(Company company : companies){
				bean = new CompanyBean();
				bean.setName(company.getCompanyName());
				bean.setCode(company.getCompanyCode());
				beans.add(bean);
			}
		}
		return beans;
	}
	
	private CompanyBean prepareCompanyBean(Company company){
		CompanyBean bean = new CompanyBean();
		bean.setCode(company.getCompanyCode());
		bean.setName(company.getCompanyName());
		return bean;
	}
}
