package com.marketanalyser.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.number.money.MonetaryAmountFormatter;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.marketanalyser.bean.CompanyDetailsBean;
import com.marketanalyser.cache.MarketAnalyserCompanyDetailsCache;
import com.marketanalyser.model.Company;
import com.marketanalyser.model.CompanyDetails;
import com.marketanalyser.service.CompanyDetailsService;
import com.marketanalyser.service.CompanyService;
import com.marketanalyser.service.ImportService;

/**
 * @author Vinod
 *
 */
@Controller
@RequestMapping(value="/analysis")
public class CompanyDetailsController {
	
	@Autowired
	private CompanyDetailsService companyDetailsService;
	
	@Autowired
	private ImportService importService;

	public static final Logger log = Logger.getLogger(CompanyDetailsController.class);
	

	@RequestMapping(value="/companies", method = RequestMethod.GET)
	public ModelAndView listCompanies() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("companies",  prepareListofBean(companyDetailsService.listCompaninyDetails()));
		return new ModelAndView("companyDetailsList", model);
	}
	
	@RequestMapping(value="/companyfilter", method = RequestMethod.GET)
	public ModelAndView filteredCompanies() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("companies",  prepareListofBean(companyDetailsService.listCompaninyDetails()));
		return new ModelAndView("companyFilter", model);
	}

	@RequestMapping(value="/importCompanyDetails", method = RequestMethod.GET)
	public ModelAndView details() throws IOException {
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("companies",  prepareListofBean(companyDetailsService.listCompaninyDetails()));
		return new ModelAndView("companyDetailsList", model);
	}
	
	
	@RequestMapping(value = "/companyfilter", method = RequestMethod.POST)
	public ModelAndView deleteCompany(@RequestParam("pricePercentage") int pricePercentage,@RequestParam("minPERatio") int minPERatio,@RequestParam("maxPERatio") int maxPERatio) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("companies",  prepareListofBean(companyDetailsService.listCompaninyDetails(pricePercentage,minPERatio,maxPERatio)));
		return new ModelAndView("companyFilter", model);
	}
	
	
	private List<CompanyDetailsBean> prepareListofBean(List<CompanyDetails> companies){
		List<CompanyDetailsBean> beans = null;
		if(companies != null && !companies.isEmpty()){
			beans = new ArrayList<CompanyDetailsBean>();
			CompanyDetailsBean bean = null;
			for(CompanyDetails company : companies){
				bean = new CompanyDetailsBean();
				BeanUtils.copyProperties(company, bean);
				beans.add(bean);
			}
		}
		return beans;
	}

}
