package com.marketanalyser.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Vinod
 *
 */
@Controller
public class ContentPageController {

	

	 
	 
	@RequestMapping("/")
	public ModelAndView home(Map<String, Object> model) {
		
		return new ModelAndView("index");
	}
	
}
