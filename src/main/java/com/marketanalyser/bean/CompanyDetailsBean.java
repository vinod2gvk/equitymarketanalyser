package com.marketanalyser.bean;

import java.math.BigDecimal;

/**
 * @author Vinod
 *
 */
public class CompanyDetailsBean {
	private String code;
	private Double peRatio;
	private Double eps;
	private Double sales;
	private Double faceValue;
	private String lastBonus;
	private Double lastDivided;
	private Double currentValue;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Double getPeRatio() {
		return peRatio;
	}
	public void setPeRatio(Double peRatio) {
		this.peRatio = peRatio;
	}
	public Double getEps() {
		return eps;
	}
	public void setEps(Double eps) {
		this.eps = eps;
	}
	public Double getSales() {
		return sales;
	}
	public void setSales(Double sales) {
		this.sales = sales;
	}
	public Double getFaceValue() {
		return faceValue;
	}
	public void setFaceValue(Double faceValue) {
		this.faceValue = faceValue;
	}
	public String getLastBonus() {
		return lastBonus;
	}
	public void setLastBonus(String lastBonus) {
		this.lastBonus = lastBonus;
	}
	public Double getLastDivided() {
		return lastDivided;
	}
	public void setLastDivided(Double lastDivided) {
		this.lastDivided = lastDivided;
	}
	public Double getCurrentValue() {
		return currentValue;
	}
	public void setCurrentValue(Double currentValue) {
		this.currentValue = currentValue;
	}
	
	
}
