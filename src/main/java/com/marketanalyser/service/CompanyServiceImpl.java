package com.marketanalyser.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marketanalyser.model.Company;
import com.marketanalyser.repository.CompanyDao;

/**
 * @author Vinod
 *
 */
@Service("companyService")
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyDao companyDao;
	
	public void addCompany(Company company) {
		companyDao.saveCompany(company);
	}
	
	public List<Company> listCompanies() {
		return companyDao.listCompanies();
	}

	public Company getCompany(String companyId) {
		return companyDao.findCompanyCode(companyId);
	}
	
	public void deleteCompany(Company company) {
		companyDao.deleteCompany(company);
	}
	
	

}
