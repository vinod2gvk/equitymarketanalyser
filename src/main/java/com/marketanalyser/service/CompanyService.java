package com.marketanalyser.service;

import java.util.List;

import com.marketanalyser.model.Company;

/**
 * @author Vinod
 *
 */
public interface CompanyService {
	
	//public void addCompany(Company company);

	public List<Company> listCompanies();
	
	public Company getCompany(String companyId);
	
	//public void deleteCompany(Company company);
}
