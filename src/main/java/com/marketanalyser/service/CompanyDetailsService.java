package com.marketanalyser.service;

import java.util.List;

import com.marketanalyser.model.CompanyDetails;

/**
 * @author Vinod
 *
 */
public interface CompanyDetailsService {
	
	public void addCompanyDetails(CompanyDetails companyDetails);

	public List<CompanyDetails> listCompaninyDetails();
	
	public CompanyDetails getCompanyDetails(String companyId);
	
	public void deleteCompanyDetails(CompanyDetails companyDetails);

	public List<CompanyDetails> listCompaninyDetails(int pricePercentage, int minPERatio, int maxPERatio);
}
