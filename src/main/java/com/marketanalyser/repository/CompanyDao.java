package com.marketanalyser.repository;

import java.util.List;

import com.marketanalyser.model.Company;

/**
 * @author Vinod
 *
 */
public interface CompanyDao{
	
	public void saveCompany(Company company);

	public List<Company> listCompanies();
	
	public Company findCompanyCode(String companyId);
	
	public void deleteCompany(Company company);
}
