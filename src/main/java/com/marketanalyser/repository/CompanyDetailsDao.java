package com.marketanalyser.repository;

import java.util.List;

import com.marketanalyser.model.CompanyDetails;

/**
 * @author Vinod
 *
 */
public interface CompanyDetailsDao{
	
	public void addCompanyDetails(CompanyDetails companyDetails);

	public List<CompanyDetails> listCompanyDetails();
	
	public CompanyDetails findCode(String code);
	
	public void deleteCompanyDetails(CompanyDetails companyDetails);

	public List<CompanyDetails> listCompanyDetails(int pricePercentage, int minPERatio, int maxPERatio);
}
