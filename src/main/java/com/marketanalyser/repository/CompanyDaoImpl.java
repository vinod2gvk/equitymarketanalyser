package com.marketanalyser.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.marketanalyser.cache.MarketAnalyserCompanyCache;
import com.marketanalyser.model.Company;

/**
 * @author Vinod
 *
 */
@Repository("companyDao")
public class CompanyDaoImpl implements CompanyDao {

	@Autowired
	MarketAnalyserCompanyCache companyCache;
	
	public void saveCompany(Company company) {
		HashMap<String,Company> companies = companyCache.getCompanyCache().get("companiesMap");
		companies.put(company.getCompanyCode(), company);
		companyCache.getCompanyCache().put("companiesMap", companies);
	}

	public List<Company> listCompanies() {
		return new ArrayList<Company>(companyCache.getCompanyCache().get("companiesMap").values());
	}

	public Company findCompanyCode(String companyId) {
		HashMap<String,Company> companies = companyCache.getCompanyCache().get("companiesMap");
		return (Company) companies.get(companyId);
	}

	public void deleteCompany(Company company) {
		HashMap<String,Company> companies = companyCache.getCompanyCache().get("companiesMap");
		companies.remove(company.getCompanyCode());
		companyCache.getCompanyCache().put("companiesMap", companies);
	}

	
	
	
}
