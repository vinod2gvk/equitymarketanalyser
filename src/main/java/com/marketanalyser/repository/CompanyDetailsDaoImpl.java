package com.marketanalyser.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.marketanalyser.cache.MarketAnalyserCompanyDetailsCache;
import com.marketanalyser.model.CompanyDetails;

/**
 * @author vinod
 *
 */
@Repository("companyDetailsDao")
public class CompanyDetailsDaoImpl implements CompanyDetailsDao {

	
	
	@Autowired
	MarketAnalyserCompanyDetailsCache companyDetailsCache;

	@Override
	public void addCompanyDetails(CompanyDetails companyDetails) {
		HashMap<String, CompanyDetails> companyDetailsMap = companyDetailsCache.getCompanyDetailsCache().get("companyDetailsMap");
		companyDetailsMap.put(companyDetails.getCode(), companyDetails);
		companyDetailsCache.getCompanyDetailsCache().put("companyDetailsMap", companyDetailsMap);
		
		
	}

	@Override
	public List<CompanyDetails> listCompanyDetails() {
		
		
		return new ArrayList<CompanyDetails>(companyDetailsCache.getCompanyDetailsCache().get("companyDetailsMap").values());
	}

	@Override
	public CompanyDetails findCode(String companyId) {
		HashMap<String, CompanyDetails> companyDetailsMap = companyDetailsCache.getCompanyDetailsCache().get("companyDetailsMap");
		return (CompanyDetails) companyDetailsMap.get(companyId);
	}

	@Override
	public void deleteCompanyDetails(CompanyDetails companyDetails) {
		
		HashMap<String, CompanyDetails> companyDetailsMap = companyDetailsCache.getCompanyDetailsCache().get("companyDetailsMap");
		companyDetailsMap.remove(companyDetails.getCode());
		
		companyDetailsCache.getCompanyDetailsCache().put("companyDetailsMap", companyDetailsMap);

		
	}

	@Override
	public List<CompanyDetails> listCompanyDetails(int pricePercentage, int minPERatio, int maxPERatio) {

		HashMap<String, CompanyDetails> companyDetailsMap = companyDetailsCache.getCompanyDetailsCache().get("companyDetailsMap");
		
		List<CompanyDetails> refinedCompanies = new ArrayList<>();
		
		for(CompanyDetails companyDetails : companyDetailsMap.values()) {
			
			if ( companyDetails.getFaceValue() < companyDetails.getCurrentValue()
					&& (companyDetails.getCurrentValue() != null && companyDetails.getFaceValue() != null && (companyDetails.getCurrentValue() / companyDetails.getFaceValue()) < 100)
					) {
				refinedCompanies.add(companyDetails);
			}

		}
		
		return refinedCompanies;
		
	}

	
	
	
}
