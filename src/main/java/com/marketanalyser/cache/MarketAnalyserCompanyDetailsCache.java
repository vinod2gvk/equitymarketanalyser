package com.marketanalyser.cache;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.jcs.JCS;
import org.apache.commons.jcs.access.CacheAccess;
import org.apache.commons.jcs.access.exception.CacheException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marketanalyser.model.Company;
import com.marketanalyser.model.CompanyDetails;
import com.marketanalyser.service.ImportService;
@Service("companyDetailsCache")
public class MarketAnalyserCompanyDetailsCache {

	@Autowired
	private ImportService importService;
	
	public CacheAccess<String, HashMap<String,CompanyDetails>> getCompanyDetailsCache(){
		 CacheAccess<String, HashMap<String,CompanyDetails>> cache = null;
	        
	        try
	        {
	             cache = JCS.getInstance( "marketanalysercompanydetailscache" );
	        }
	        catch ( CacheException e )
	        {
	               System.out.println("Cache is not initialised!!!");
	        }
	        
	        
	        if(null == cache.get("companyDetailsMap") || cache.get("companyDetailsMap").isEmpty() ) {
	        cache.put("companyDetailsMap", importService.importCompanyDetails());
	        }
	        
	        
	        return cache;
	}
}
