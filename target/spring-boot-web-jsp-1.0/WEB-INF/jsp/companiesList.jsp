<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>All Companies</title>
</head>

<style>
  * {
   box-sizing: border-box; 
  }
  body {
    margin: 0;
  }
  #main {
    display: flex;
    min-height: calc(100vh - 40vh);
  }
  #main > article {
    flex: 1;
  }
  #main > nav, 
  #main > aside {
    flex: 0 0 20vw;
    background: beige;
  }
  #main > nav {
    order: -1;
  }
  header, footer, article, nav, aside {
    padding: 1em;
  }
  header, footer {
    background: yellowgreen;
    height: 20vh;
  }
</style>
<body>
  <header><%@include file="header.jsp" %></header>
  <div id="main">
    <article>
    <h1>List Companies</h1>

<c:if test="${!empty companies}">
	<table align="left" border="1">
		<tr>
			<th>Company Name</th>
			<th>Company Code</th>
			
		</tr>

		<c:forEach items="${companies}" var="company">
			<tr>
				<td><c:out value="${company.name}"/></td>
				<td><c:out value="${company.code}"/></td>
				
			</tr>
		</c:forEach>
	</table>
</c:if>
    
    </article>
    <nav><%@include file="navigation.jsp" %></nav>
    <aside><%@include file="widget.jsp" %></aside>
  </div>
  <footer><%@include file="footer.jsp" %></footer>
</body> 
</html>